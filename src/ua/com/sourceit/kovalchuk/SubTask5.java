package ua.com.sourceit.kovalchuk;

import java.util.Scanner;

public class SubTask5 {

	public static void main(String[] args) {
		int number;
        if (args.length == 0) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter number: ");
            number = sc.nextInt();
        } else {
            number = Integer.parseInt(args[0]);
        }

        System.out.print("Simple numbers: 2");
        for (int i = 3; i <= number; i++) {
            boolean isSimple = true;
            for (int j = 2; j < i; j++) {
                if ((i % j) == 0) {
                    isSimple = false;
                    break;
                }
            }
            if (isSimple) System.out.print(", " + i);
        }
        System.out.println();
	}

}
