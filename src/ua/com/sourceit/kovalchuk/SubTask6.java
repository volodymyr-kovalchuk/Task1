package ua.com.sourceit.kovalchuk;

public class SubTask6 {
	
	public static final int CODE_LETTER_A = 65;
    public static final int CODE_LETTER_Z = 90;
    public static final int NUMBER_OF_LETTERS = CODE_LETTER_Z - CODE_LETTER_A + 1;

	public static void main(String[] args) {
		System.out.println("The sequence number of \"AAB\" column = " + transformLettersToNumber("AAB"));
        System.out.println("Letters that correspond to the sequence number of 1379: " +
                transformNumberToLetters(1379));
        System.out.println("The sequence number of right column after \"X\" column = " +
                getRightColumnNumber("X"));
    }

    private static int transformLettersToNumber(String letters) {
        int generalSequenceNumber = 0;
        for (int i = 0; i < letters.length(); i++) {
            int sequenceNumberOfLetter = letters.toUpperCase().charAt(i) - CODE_LETTER_A + 1;
            generalSequenceNumber += sequenceNumberOfLetter *
                    (int) Math.pow(NUMBER_OF_LETTERS, (letters.length() - i - 1));
        }
        return generalSequenceNumber;
    }

    private static String transformNumberToLetters(int sequenceNumber) {
        StringBuilder letters = new StringBuilder();
        do {
            int b = (sequenceNumber - 1) % NUMBER_OF_LETTERS;
            letters.insert(0, (char) (b + CODE_LETTER_A));
            sequenceNumber = (sequenceNumber - 1) / NUMBER_OF_LETTERS;
        } while (sequenceNumber > 0);
        return letters.toString();
    }

    private static int getRightColumnNumber(String letters) {
        return transformLettersToNumber(letters) + 1;
    }

}
