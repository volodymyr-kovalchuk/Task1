package ua.com.sourceit.kovalchuk;

import java.util.Scanner;

public class SubTask4 {
	
	public static final int DIVIDER = 10;

	public static void main(String[] args) {
		long number;
        if (args.length == 0) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter start number: ");
            number = sc.nextLong();
        } else {
            number = Long.parseLong(args[0]);
        }
        int resultNumber = 0;
        while (number != 0) {
            resultNumber += number % DIVIDER;
            number /= DIVIDER;
        }
        System.out.println("The sum of individual digits of an integer = " + resultNumber);
	}

}
