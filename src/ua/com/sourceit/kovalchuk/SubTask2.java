package ua.com.sourceit.kovalchuk;

import java.util.Scanner;

public class SubTask2 {

	public static void main(String[] args) {
		StringBuilder result = new StringBuilder();
        if (args.length == 0) {
            Scanner sc = new Scanner(System.in);
            boolean isEmpty, isFirst = true;
            do {
                System.out.println("Please enter strings to form the text-line (enter a blank string to end):");
                String entering = sc.nextLine();
                if (entering.length() == 0) {
                    isEmpty = true;
                } else {
                    if (!isFirst) result.append(" ");
                    result.append(entering);
                    isEmpty = false;
                    isFirst = false;
                }
            } while (!isEmpty);
        } else {
            for (int i = 0; i < args.length; i++) {
                if (i !=0) result.append(" ");
                result.append(args[i]);
            }
        }
        System.out.println("\"" + result + "\"");
	}

}
