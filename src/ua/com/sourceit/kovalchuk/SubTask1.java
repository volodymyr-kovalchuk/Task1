package ua.com.sourceit.kovalchuk;

import java.util.Scanner;

public class SubTask1 {

	public static void main(String[] args) {
		double firstNumber, secondNumber;
        if (args.length != 2) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter first number: ");
            firstNumber = sc.nextDouble();
            System.out.println("Enter second number: ");
            secondNumber = sc.nextDouble();
        } else {
            firstNumber = Double.parseDouble(args[0]);
            secondNumber = Double.parseDouble(args[1]);
        }
        System.out.printf("Sum of entered numbers = %.2f \n", (firstNumber + secondNumber));
	}

}
