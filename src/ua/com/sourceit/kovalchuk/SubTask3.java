package ua.com.sourceit.kovalchuk;

import java.util.Scanner;

public class SubTask3 {

	public static void main(String[] args) {
		double firstNumber, secondNumber;
        if (args.length != 2) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter first number: ");
            firstNumber = sc.nextInt();
            System.out.println("Enter second number: ");
            secondNumber = sc.nextInt();
        } else {
            firstNumber = Integer.parseInt(args[0]);
            secondNumber = Integer.parseInt(args[1]);
        }
        int startNumber = (int)Math.min(Math.abs(firstNumber), Math.abs(secondNumber));
        for (int i = startNumber; i > 0; i--) {
            if ((firstNumber % i == 0) && (secondNumber % i == 0)) {
                System.out.println("NSD=" + i);
                break;
            }
        }
	}

}
